Title: 前言
Slug: preface
page_order: -2

[TOC]


## 這是一本什麼樣的書？

正如本書副標題所示，這是一本手冊，但不會像傳統技術手冊一樣教導一套既定的操作模式。[^instructions] 閱讀本書的目的不單純只是學寫程式，亦非只是對寫程式的實務本身進行批判性反思，本書提供了一些更為混亂，但同時也讓我們覺得更加「實用」的東西：一本關於寫作、編碼和思考之間，更為錯綜複雜的關係的書。[^useful]

多數程式設計書籍的主要目標，是告訴讀者該如何學習程式語言，以成為一名優秀（或更好）的程式設計師，並著重於最先進的技術，以及經過解釋與設計，以便利用在科技相關或創意產業的實際範例。透過程式設計的文化和美學面向來進行批判性思考和行動的書籍並不多見。[^guzdial] 與運算技術有關的軟體研究、平台研究，以及部分數位人文學科等新興跨學科領域，都已將程式設計實作納入研究對象，但在如何以批判性觀點切入程式設計研究方面，實際的細節卻很有限，對於研究非科技或非科學學科領域的人而言，這點尤為明顯。人們對「運算式思維」[^ct] 的興趣日益增長，期望將之擴展到電腦科學或軟體工程之外的範疇（甚至超越數位人文學科，因為這個領域最終亦會帶來另一組限制），但現有文獻對這個主題的討論卻相形匱乏，而本書試圖弭平的，正是這兩者之間的落差。在這方面，我們採取的方法與其他這方面的書籍或其他理論書籍迥然不同，在那些書籍中，原始碼會成為程式設計師思維的展現，或是針對隱藏的作業層面過於簡單的類比（若沒有被完全忽略的話）。

廣泛來說，本書不僅呼應了軟體研究的精神[^Fuller]，同時也在認知到我們的經驗日益程式化的情況下，提供一種注重實務的應用式途徑，來理解程式設計（以軟體閱讀、寫作和思考）作為這個時代的關鍵工具的重要性。本書以最開闊的視角，提供深度學習的「工具」，是一本特別為不熟悉程式設計的人量身打造的手冊，讓你可以隨著技術愈發熟練，發展出程式設計師的概念性技能。

再重申一次，我們的目的是協助讀者掌握關鍵的程式設計技能，以便透過程式碼來閱讀、編寫和思考（稍後我們將回到編碼讀寫能力的議題）。我們認為，進一步探討程式碼的技術和概念面向的交集會是一大重點，藉此我們可以深入反思運算文化的普遍性，以及這種文化的社會和政治性影響，如人機語言、物件抽象化、資料化，和自動化機器智慧的最新發展等。換句話說，這本書既涵蓋了程式碼的技術層面和形式特質，也開啟了對程式碼的想像，包括承認程式設計實務操作的物質條件、程式碼本身的非人能動性，以及其在更廣泛的生態系中所固有的內在關係性。  

除了協助讀者學習程式設計之外，我們亦希望能消除運算文化、美學和文化研究理論之間的鴻溝。其中，我們對大眾尚未充分認知的權力關係特別感興趣，例如與階級、社會性別和生理性別相關的不平等和不公義，以及種族議題和陰魂不散的殖民主義。[^decolonial] 這不僅關乎我們自身的主體地位和文化背景（更多相關資訊，請參閱作者簡介）和再現政治（主體與主體再現之間的爭議空間），還關乎權力差異如何隱含在程式碼的二元邏輯、層級、屬性命名、無障礙功能等方面，以及更廣泛的社會不平等情形，又是怎麼透過非再現性的運算，進一步得到強化和延續。簡言之，這本手冊介紹並示範了一種被我們稱為「美學程式設計」的反思式實踐，這種獨特的方法在被我們解釋並執行的同時，也逐漸形塑了我們的主體性。
{: style="word-spacing: -0.2px;"}

## 為何要探討美學程式設計？

本書依循的論點是，運算文化不僅是用來提升解決問題和分析能力的熱門科目，或是一種了解更多關於運算過程中所發生之事的方法，更是一種透過親身參與程式設計，從而質疑現有技術典範，並進一步改變技術系統的手段。因此，我們認為程式設計是一種動態的文化實踐和現象、一種在世界上思考和做事的方法，以及一種理解某些複雜程序的手段，這些程序支持並建構了我們的生活現實，藉由理解它們，我們便能根據這些現實採取行動。

在我們看來，「美學程式設計」這個詞可以妥適地描述這種方法。我們聲稱自己在這方面具備一定的獨特性，不過美學程式設計當然也與近年來相關文獻中引介的其他術語，例如「創意編碼」和「探索式程式設計」相當接近，都是在強調電腦程式設計超越了實用性和功能性，人們可以從藝術與人文的廣闊視角，透過程式設計的實踐發展出文化生產或批判性思維。[^Refs] 在此我們應該說明一下，「美學程式設計」這個名稱，其實源自於丹麥奧胡斯大學（Aarhus University）的一門數位設計學士學程，該課程自 2013 年起，便一直與軟體研究課程並行授課。按照設計，綜合這兩門課程，便可以學到一些與軟體共同思考的方式，從而理解更廣泛的政治和美學現象。[^students] 本書所遵循的原則是，由於軟體的重要性日益顯著，我們需要一種新的文化思維和課程，來解釋並從內部更清楚地理解演算法程序、資料處理和抽象模型的政治和美學。本書的架構主要來自教授這些課程的經驗，而我們要在此感謝所有學生和教授一路以來的寶貴貢獻和指教反饋。[^people]

延續關於美學的討論，我們應該澄清，此處所指涉的美學不是一般常被誤解的美的觀念（又名資產階級美學），而是政治性美學，若引用賈克·洪席耶（Jacques Rancière）的《美學與政治》（The Politics of Aesthetics，本書的主要參考資料之一），並以我們的方式重述，就是一種將自己呈現在意義建構經驗和身體感知中的東西。[^Ranciere] 我們在此種政治意義上看待世界的方式並非恆常固著，而是不斷變化，就像軟體本身一樣，受制於更廣泛的條件和生態。

政治美學一詞可上溯至法蘭克福學派的批判理論，尤其是狄奧多・阿多諾（Theodor Adorno）和華特・本雅明（Walter Benjamin）的觀點，上述思想強化了看待文化生產（如今自然也涵蓋程式設計）必須結合社會脈絡的概念。在這樣的理解方式下，程式設計成了一種「力場」，透過它，我們得以瞭解物質條件和社會矛盾，就如同對藝術的詮釋曾經「作為一種語言的符碼，用於處理社會中發生的事情」。[^Jay] 本雅明被多次援引的技術再現性相關文章已成為一顆打破藝術生產神話的試金石，其中包括卸除「靈光」（Aura，彰顯藝術品本真性和原創性的標誌）這種審美體驗。[^Benjamin] 同樣值得追溯的是，阿多諾和本雅明針對靈光破滅所造成的後果存有著名的歧見：本雅明表示這種轉變有著正向意義，並將靈光的破壞視為一種政治解放，反之，阿多諾則持消極看法，認為標準化和「偽個性」等負面影響會隨之而來。兩種趨勢看來似乎皆已隨著運算文化而加劇，因此需要持續進行尖銳的批判，而這種批判是基於固有的「內在批評」的途徑，於物件內部（亦即軟體內部運作以及其物質條件中）運行。[^Adorno] 然而，這些舊有（由白人男性撰寫的）參考文獻，到底能在多大程度上勝任拆解運算作業複雜性的任務，並針對大多數人使用或思考電腦的方式提出洞見，這仍然是個未解的問題。上述這點除了關乎美學程式設計正在成為什麼，同樣亦關乎它現在或曾經是什麼，以及為何我們明明意識到這些參考文獻本身長期存在問題，並且亟需替代方案，卻仍要以它們作為出發點。

若一切都不斷變化，如同其表象所呈現那般，那麼我們也會需要對大眾普遍接受的方法保持敏銳覺知，畢竟這些方法是基於西方的知識生產傳統和進化的概念，並根植於歐洲殖民主義和壓榨行為。[^afro] 我們發現，安娜・羅文豪普特・秦（Anna Lowenhaupf Tsing）的相關著作在這方面相當實用，這是因為她的著作提供了一種女性主義觀點，以更認真的態度看待不確定性，從而反映了生活條件的不穩定。[^Tsing] 與此密切相關的，不僅只有我們的歷史和技術理論，還有批判性實踐及其他非人類的想像。正如秦所言，其他生態系開闢了多向、跨越時間和尺度、更加開放而不確定，並且基於「聚合」（assemblage）觀念的思維方式。[^Tsing2] 不像她自蘑菇中尋覓靈感，我們是從技術本身的內在和相關品質中獲得啟發。更精確來說，我們一直在研究基本的程式設計概念，如幾何和物件抽象化；迴圈和時間性；資料和資料化；變數、函式及其命名，以及資料擷取、處理和自動化等，作為更深層的美學反思的起點，技術功能則為進一步理解文化現象的建構和運作方式奠定了基礎。

從這個意義上來說，美學程式設計可被視為一種建構事物、創造世界的實務做法，但同時也產生了對電腦科學、藝術和文化理論的內在批判。另一方面，這也和菲利普・阿格雷（Philip Agre）所提出，將形式技術邏輯和論述文化意義結合的「批判性技術實踐」概念非常接近。[^agre] 換言之，美學程式設計這種思考方式，需要對程式設計有實質的理解與知識，方能支持對技術文化系統的批判性認識，而這亦須以上述兩個領域的專業學識水準為基礎，例如全喜卿（Wendy Hui Kyong Chun）和許煜（Yuk Hui）兩位學者，便具備這樣的跨學科專門知識。[^Chun] 這種實際的「知識」也展現了「做中思考」的實踐[^britton]，擁抱與程式設計合作的多種方式，以探索寫作、編碼和思考之間的關係，進而想像、創造並提出「替代方案」。[^agre1] 透過上述方式，我們參與美學程式設計，以期能讓批判性和技術性實務做法的交集更加「酷兒化」[^queer]，正如本書中援引的多項示例一般（參見〈計畫列表〉，此外，作為經典範例，全書從頭到尾皆引用孫詠怡（Winnie Soon）的著作），並望能進一步討論技術主題中，相對未獲認知的權力關係。我們希望藉此鼓勵愈來愈多的人們起身挑戰各種分化的領域和實務操作。 
{: style="word-spacing: -0.5px;"}

## 那軟體研究呢？

如上所述，透過分析各式軟體產出物，並仔細閱讀理論性文本和原始碼，我們探討了作為一種文化形式的軟體知識，並將之傳達給人們，在此我們大幅仰賴軟體研究領域，並在一定程度上利用批判性程式碼研究，如全喜卿（Wendy Chun）、馬修・富勒（Matthew Fuller）和馬克・馬利諾（Mark Marino）等人的論著，以及我們自己早期的著作。本書所採用之方法靈感主要來自富勒 2008 年出版的專著《軟體研究辭典》（Software Studies: A Lexicon），一本確實採取關鍵術語辭典的形式為架構的書，而此著作則參考了雷蒙・威廉斯（Raymond Williams）於1958年首次出版的《關鍵詞：文化與社會的詞匯》。[^Williams] 簡言之，我們的書在很多方面都採用了類似的方法，亦即拉近距離觀察運算的形式邏輯，並拉遠距離審視軟體的文化含義。在這方面，認識《軟體研究辭典》一書的撰寫脈絡亦相當重要，它源自一場臨時工作坊，而其明確的計畫方針亦值得於下文引述：

> 「【這項】計畫旨在將科學研究的內在主義/外在主義問題裡外互反，而將一個主題與另一個主題結合的機制如下：由軟體所支持的學術研究，在軟體、藝術和文學實踐等領域，針對軟體這個媒介有何欲言說之處？因此，上述互動的目的，並不是為了揭示軟體所謂隱藏的真相，顯露其深奧難解的現實，而是一窺軟體究竟是什麼，以及它可以與什麼相結合：這個富含矛盾連結的接縫，正是運算的速度和合理性與其表見的外部相交會之處。」[^ss]

我們相信，關注程式設計中的基本或關鍵概念。可以在美學和批判理論方面開拓新的洞見，並為與運算邏輯關聯日深的文化現象帶來嶄新觀點。如此一來，儘管意識到我們與問題共存，但我們仍認為，從軟體的內部運作及其物質條件來看，以這樣的方式工作非常重要。藉由將討論擴展到形式邏輯之外的領域，我們亦強調了藝術實踐在開拓更多思辨性、替代性和混亂想像方面的實用性。本於此精神（或至少是本於與歐洲軟體研究的發展保持同調的想法），我們自所謂的「軟體藝術」（儘管該類別的意義僅僅只是個佔位符）[^placeholder] 或「運算藝術」中汲取靈感[^Readme]，並以藝術（和批判性設計）實踐中的示例作為論證的一部分，從而強調我們的觀點，亦即程式設計不僅只是一種產生藝術作品的實用工具，其本身更是一種批判性審美的物件或對象。[^codestudies] 策展人英克‧阿恩斯（Inke Arns）巧妙地闡釋了這一點：

> 「『軟體藝術』（中略）係指能夠以軟體為媒介或材料，反映軟體（及其文化意義）的藝術活動。此等藝術並不會將軟體視為隱沒在其創造之產品背後的實用輔助工具，而是聚焦其所包含的程式碼，即使這類程式碼並不一定會被明確顯示或強調亦然。據弗洛里安‧克拉默（Florian Cramer）所言，軟體藝術讓看似中立的技術指令序列之美學和潛在政治意義變得清晰可見。軟體藝術的基礎，可以建立於原始碼、抽象演算法，或由給定之程式碼片段創造的產品等諸多不同的軟體層次。」[^Arns]

我們在本書所主張的論點與上述相當類似，此外，為了探討程式碼和運算過程的美學面向，我們將納入探索軟體的物質條件和運算過程的藝術作品，作為實踐與理論的範例。換句話說，這些例子是我們的論證中不可或缺的一部分，並且能透過實務展示一些觀點，並帶來意料之外的認識論見解。我們或可在此補充一點：重述上文已經引介過的內容，我們感興趣的對象，並不僅是程式設計的批判性美學，更是作為批判性美學的程式設計本身。


## 開放授權出版

更扼要來說，本書的程式碼中有文字（以人類能夠讀懂的形式呈現），文字中有程式碼（我們用以呈現上述想法的文本編輯器、界面和線上平台）。關於這一點，我們還有更多話想說，因此我們將在本書各章之中回顧這些議題，而書中的每一個章節，皆遵循基本程式設計的邏輯概念。當前唯一需要點明的是，本書的目的為表達寫作和編碼之間深切的糾纏，以及兩者之間並無孰高孰低，不應獨厚一方，我們可以從它們之間的關係中學習。寫程式和撰寫程式相關內容兩件事被迫以某種方式結合在一起，以反映資料實踐和開放授權出版計畫中，更為廣泛的文化和技術變遷，並強調一本書的撰寫工作必然是一項現在進行式。就像軟體一樣，這會是一本可供閱讀、執行、共享和重寫的書籍。

顯然，這種公開協作式的軟體生產方法有許多先例，而免費和開源兩項原則更凸顯了我們的想法。值得強調的是，自由及開放原始碼軟體（Free and Open Source Software，縮寫為 FOSS）的開發是一種集體實踐，挑戰了與商業化發展相關的標準性生產關係，如狹義的作者身分和版權，以及固定的分工模式等，這種生產關係還可延伸到書籍的生產和學術出版相關之聲譽經濟。然而，我們也意識到，原始碼和開放授權書籍的釋出，涉及了關乎共享經濟、自由市場資本主義，以及利用免費勞動力來獲利等諸多模糊地帶。我們期望，持續的堅持和努力能夠挑戰過度簡化的邏輯，而本書英文版的出版商「開放人文出版社」（Open Humanities Press）也透過投入學術研究的開放使用，廣泛反映了 FOSS 的透明度和可複製兩項原則。[^OHP] 因此，讀者將可免費下載本書，或以合理的價格購買紙本書。

這種做法並無特殊或原創之處。我們知道，世上還有許多其他實驗性出版計劃，甚至還有致力於讓文章出版過程去中心化的「反平台」。[^dok] 此外尚有許多其他例子，指出了撰寫程式設計相關書籍的悖謬之處：讀者必須先輸入範例，才能開始執行程式，而即時編碼平台則展示了替代方案（如 Jupyter Notebook 或 DevDocs[^devdocs]）。為了處理這個問題，我們使用了印刷技術以及相關的軟體儲存庫，並以此為指引，來選擇本書的設計者——開源出版小組（Open Source Publishing collective）的設計僅使用免費和開源之軟體（正如這間公司所宣稱，「這些軟體會邀請用戶合作參與設計」[^OSP]），並透過包含所有計劃相關檔案的 Git 版本控制系統，免費提供所有檔案，系統中的檔案皆根據 GNU 通用公眾授權條款（第二版）散佈。下一章將對此進行更詳細的介紹。

簡言之，藉由以Git儲存庫寫作本書，我們進一步強調了 FOSS 的作業原則，而透過將寫作視為軟體，或者可說是將軟體視為寫作，我們得以將本書的製作形式化，成為一個需要及時更新、允許分叉（forking）和無限改版的迭代過程。我們希望藉著允許他人製作新版本，適度地挑戰商業出版慣例，並透過利用基礎設施將想法化為編碼並發佈在網路上，來展現我們理解這些基礎設施的能耐。我們相信，這種工作方式標誌著集體參與程式設計，以及在社會技術系統（內部和外部）中帶來改變的起點。我們的目標與阿德里安‧麥肯齊（Adrian Mackenzie）所認定的「自考古學」（auto-archaeology）相似，指出研究的對象如何被完全整合到分析之中，麥肯齊於2017年出版之《機器學習者》（Machine Learners，暫譯）裡，已經利用相關的 GitHub 站點演示了這一點。[^Mackenzie] 這可以幫助讀者理解，本著軟體開發人員協作、託管、審查並合併程式碼，以及共同建構軟體的精神來撰寫一本程式碼相關書籍，是怎麼樣的迭代過程。[^Git]

## 內容次序

本書中主要使用的程式語言是 JavaScript，並側重以網頁為基礎的開源資料庫 p5.js，它為程式設計初學者提供完整且易於上手的學習資源（在第一章〈入門〉中將詳細說明）。我們的資源結合了完成關鍵工作所必要的技術知識取得、學習程式設計最廣義來說涵蓋的所有範圍，以及作為實作、思索，以及世界構成（world-making）的程式設計。

本書每一章的開頭都會放上一張流程圖[^graphviz]，以「圖表」呈現該章節內容的形式／技術和美學／概念等方面之間複雜而糾纏的關係。每張流程圖都透過具體和抽象兩種方式捕捉動態（學習）的過程，以利讀者思考圖表各元素之間的關係，特別是「setup()」（譯按：本書中的函式皆保留原文，括號中的內容則依情況而定）部分的舞台設置[^setup]、「start()」所表達的起始點、「原始碼」中展示之產出物和範例、「課堂練習和討論」中列出之程式碼和語法的形式和論證，以及「While()」中進一步的延伸討論。既然流程圖通常被當作一種輔助工具，可用來設計運算過程，並增進技術層面的理解和傳遞[^diagram]，那麼我們也認為，這個程序亦應包含批判性反思。事實上，以流程圖為起點，正說明了我們將概念「裡外反轉」的方法，以及透過邏輯和推論的形式，來理解運算和程式設計中的物件、關係和程序之必要。

從第一章〈入門〉開始，我們說明了使用本書所需的工具和環境。本章將引導你編寫第一個程式，並藉此思考「讀寫能力」（literacy）這項概念，此處提及的讀寫能力，包含了編寫程式碼（RunMe）和撰寫程式碼相關內容（ReadMe）兩個面向。第二章〈變數幾何〉藉由探討變數的應用，以及形狀、大小、位置、空間等幾何繪製，來連接表情符號的相關討論，和它們所喚起的再現政治。第三章〈無限迴圈〉，利用不同的轉換語法，將焦點從靜態物件轉移到動態物件。透過預載入的轉輪圖示，我們將學習條件運算式、迴圈和時間相關語法，以反思科技如何在我們的時間感知中扮演關鍵角色。第四章〈資料擷取〉介紹程式如何擷取並處理輸入的資料（經由音訊和網路攝影機裝置、滑鼠和鍵盤按鍵，以及可自訂和點按的按鈕），並進一步討論了資料擷取和資料化的影響。第五章〈自動產生器〉重點介紹規則和指令如何執行，以及它們如何在產生系統中產生意想不到和／或複雜的結果，並製造出突現的／酷兒的[^queer2] 生命形式。從這部分起，本書在技術方面將更趨複雜，第六章〈物件抽象化〉介紹了物件導向的程式設計，其中說明物件可自訂的屬性和行為，同時亦討論抽象現實與具象現實之間的移動。第七章〈言說程式碼〉密切觀察命名和運算的架構，探索程式碼與自然語言之間富有詩意的關係。藉由特別關注語音，我們反思人類主體如何涉入編碼實務，以及編碼本身如何表達更廣泛的政治議題。利用應用程式設計介面（API）的即時查詢功能，第八章〈酷詢資料〉仔細審視請求和取得資料的方式，探索資訊的處理如何結合資料的選擇、標準化、提取、傳送和呈現。我們藉此獲得的洞見，包括由查詢驅動的社會政治，以及搜索引擎如何以折衷形式生產知識。至此本書進入最後部分，第九章〈演算法過程〉的焦點並非程式碼的語法，而是關注我們如何將演算法的程序運作拆解為一種邏輯的形式。本章的流程圖，是展示演算法的實務和概念層面的一種手段，並可用以探索流程化運作的政治效應（如前所述，本書每個章節都利用流程圖這個策略，以圖表作為章節的指引地圖）。最後一章〈機器反學習〉介紹機器學習演算法，從而普遍探討訓練、教學和學習的含義，整合各章提到的諸多議題，並進一步反思這本書做為學習工具的目標。但這並不僅止於說教，因此，我們帶來一個額外的章節〈後記：遞迴的想像〉，這是以本書內容為基礎，以機器產生的章節，涉及讀者已經學到了什麼，還有所學到的東西有哪些或許可以被忘卻。總之，本書前六章為理解程式設計的基本要素奠定了基礎，後四章則在此基礎上建構了更具推想性的形式和想像。
{: style="letter-spacing: 0.1px;"}

創作實踐在我們所採取的方式中，是最為重要的先導。每章都附有一個利用不同語法的範例，以便促成形式方面的程式碼相關討論。但除此之外，我們在教學中也碰到過學生（尤其是初學者）難以將不同的函式結合在同一個草圖的狀況。雖說本書每一章的內容都建立在其他章節的基礎上，但我們也發現，重複使用語法，並展示如何以多種方式組合和使用不同的函式和語法也很重要。大部分的個別函式，都可以從p5.js參考頁面中，或者其他線上教學影片[^shiffman] 中找到，然而我們要強調的，是將製作與思考結合的重要性。發想書中範例時，我們有將上述這點納入考量，並藉此增加各章節的複雜性，而為了便於讀者理解，每個草圖都包含了大約一百行程式碼。應該說明的是，我們在這裡並沒有優先考慮高效的程式碼，有時也會相應更改預期的語法使用方式。事實上，我們的目標並非提供核心p5.js函式的完整列表，就像許多程式設計書籍一樣，而是根據自己選擇的範例，提供解釋語法的方式，並注重這些範例是否激發進一步的討論。我們的範例靈感源自其他藝術家、設計師、科學家和理論家，並根據人們所感知的學習步調和這些範例的批判潛力，並結合各章節所重點研究的語法和功能進行客製化設計。因此，我們提供這些範例，是希望能在課堂內外激發技術和概念方面的討論，其中包括我們為每章制定的迷你習作（又名「miniX」）。

我們利用「RunMe」（編寫）和「ReadMe」（解讀）雙重元素[^dual]，讓學生透過寫程式和書寫程式碼相關內容，來培養反思能力，並鼓勵他們對所學知識如何應用在產出物的製作上提出質疑。以「低地板（低門檻）、高天花板（難度循序漸進）」為教學原則[^papert]，學生可以使用本書材料輕鬆開始學習，但每個迷你習作也藉由公開討論這些主題，而非追求既定的結果，來鼓勵學生獨立學習。我們在各個章節中引入了幾套不同的開放性任務及問題，以測試在某特定時間點已知內容的極限。在實務和迭代學習方面，我們也在每個迷你習作中納入同儕反饋，以鼓勵學員透過互相閱讀彼此的程式碼來進行學習[^readcode]，並進一步強調方法的多樣性以及公開分享想法的價值，就像一道共享的食譜一般。書中以烹飪為喻，進行概念上的比較，如將變數比擬成廚房中的容器、把演算法喻為料理步驟等。我們從本書設計團隊「開源出版社」（Open Source Publishing）汲取靈感（他們在為初學者舉辦研討會方面擁有豐富的經驗），並採用他們所使用的廚房隱喻，將編碼和烹飪、品嚐和測試的實務結合起來，邀請人們對各種成分進行進一步實驗：「在 OSP 的廚房中，原始檔案=成分。」[^kitchen]

「處境知識」的原則引導了我們所試圖概述的內容，因為我們提供的內容反映了這類知識的特定生產條件，以及知識生產者（我們自身和一起合作過的學生）的處境性。[^situatedness] 具體而言，在過去五年間，這本書透過實際的課堂教學和作業，在極為特定的文化和教育脈絡下發展，並定位於聚焦軟體研究的人文學科教育學位課程。修課之前，學生大多沒有程式設計經驗。課程分為十四堂，每週共計有八小時美學程式設計，以及三小時軟體研究的學習時數。本書雖有十個主要章節，但在實務上[^teaching]，我們提供了一個「休息週」以減緩課程步調，期間並不會介紹新的函式和語法，而是讓學生重新審視先前學到的函式和語法、討論是什麼構成了美學程式設計，以及這些如何與他們每週的演練和學習連結。我們另設有計畫週，以為最終的成果評估發表做準備。若欲依照本書安排課程教案，我們建議你要適度地靈活調整，因為很顯然，本書希望能讓讀者根據自身目的和工作環境的特殊性，以適應、修改和重新安排學習內容。我們的內容已開放資源的形式提供，從而開拓不同的可能性，讓讀者得以在各種材料和想法之間進行「剪輯」[^barad]，並鼓勵讀者分岔、複製及量身打造自己的版本，舉例而言，他們可以引用不同的參考資料、範例、反思，並創造新的章節。[^fork] 因此，引述珍尼佛・加布里斯（Jennifer Gabrys）的話，我們的計畫是「一項邀請，引領大家去製作、組織、策畫、召喚並維持人類、科技與世界，從而走向開放，而不是依循已知的結果。」[^gabrys]


## 本書作為一項物件

最後，我們想要強調，當你看到這段文字時，這本書不僅是一個你目前可能正握在手中的實體，更是一個具備運算性和網路連結的物件，散佈在其他各種時空之中，供讀者和書寫者取用。在提及這點時，我們又再次參考了本雅明的論點，他在〈作家作為生產者〉一文中寫道：「就成為一位進行描述或發出指令的人這層意義上來說，讀者總是準備好要成為一位寫作者。（中略）而撰寫與工作相關的文章因此成了完成該工作的必要技能之一。寫作的權威不再建立於專業訓練之上，而是建立在多種技術訓練中，因此寫作便成了一項共有財產[^Benjamin2]。」有趣的是，無論是對本雅明，還是對這本書而言，文化生產都需要具備一種教育上的功能。

這正是我們的觀點。這本書將自身表現為一個動態物件，不具固定的屬性、商品形式，或特定的確切性質。循著這條路徑，雖然前言是本書的開始，但終點可能並不存在：這本書有目的地陷入了它自身形成的無限迴圈之中。


## 註釋

[^instructions]: 許多電腦教學書籍經常會使用「傻瓜書」（for dummies）這個輕鬆的詞彙來稱呼，但這個詞帶有一點學習障礙的貶義。

[^useful]: 本書希望提供的「實用知識」是一種非標準式的讀寫能力，參見：Marilyn M. Cooper, "Really Useful Knowledge: A Cultural Studies Agenda for Writing Centers," *The Writing Center Journal* 14, N°2 (Spring 1994): 97-111, <https://jstor.org/stable/43441948>.

[^guzdial]: Mark Guzdial 認為，程式設計除了常規關注的經濟效益以及軟體產業的專業人才發展外，還有其他的目的，這為學習和教授程式設計開拓了多種可能性。參見：Mark Guzdial, "Computing for Other Disciplines," in *The Cambridge Handbook of Computing Education Research*, Sally A. Fincher and Anthony V. Robins, eds. (Cambridge: Cambridge University Press, 2019), 584, <https://doi.org/10.1017/9781108654555.020>.

[^ct]: <span style=" word-spacing: -0.3px; letter-spacing: -0.1px;" markdown=1> Seymour Papert 在 1980 年出版頗具影響力的著作《Mindstorms》，他將運算導入兒童的學習。創造「運算式思維」一詞，利用早期程式設計語言 Logo（該語言並不只將軟體當作工具使用）強調流程的建構，試圖弭平數學、科學與教育文化以及社會批判之間的斷層。Donald Knuth 在《Literate Programming》一書中更釐清了讀寫能力的概念，並納入一種程式設計相關文獻，其中涉及將程式語言視為人類讀者所使用的自然語言。近年來，軟體研究領域的學者亦採納運算式思維的概念，如 David Berry、Matthew Fuller、Nick Montfort 和 Annette Vee 等人。參見：Seymour Papert, *Mindstorms; Children, Computers and Powerful Ideas* (New York: Basic Books, 1980); Donald Ervin Knuth, *Literate Programming* CSLI Lecture Notes, N°27 (Stanford, CA: Center for the Study of Language and Information, 1992); Jeannette M. Wing, "Computational Thinking," *Commun. ACM* 49, N°3 (March 2006): 33–35; Michael, Mateas, "Procedural Literacy: Educating the New Media Practitioner," *Horizon* 13, N°2 (June 1, 2005): 101–11; David M. Berry, and Anders Fagerjord, *Digital Humanities: Knowledge and Critique in a Digital Age* (Hoboken, NJ: John Wiley & Sons, 2017); Matthew Fuller, *How to be a Geek: Essays on the Culture of Software* (Cambridge: Polity Press, 2017); Nick Montfort, *Exploratory Programming for the Arts and Humanities* (Cambridge, MA: MIT Press, 2016); Annette Vee, *Coding Literacy: How Computer Programming Is Changing Writing* (Cambridge, MA: MIT Press, 2017); Ole Sejer Iversen, Rachel Charlotte Smith and Christian Dindler, "From computational thinking to computational empowerment: a 21st century PD agenda," *Proceedings of the 15<sup>th</sup> Participatory Design Conference Full Papers - Volume 1*, N°7 (August 2018): 1-11.</span>

[^Fuller]: 軟體研究是跨學科的研究領域，研究軟體及其社會和文化影響，參見：Matthew Fuller, ed. *Software Studies: A Lexicon* (Cambridge, MA: MIT Press, 2008).

[^decolonial]: 舉例而言，請參閱：Syed Mustafa Ali, "A Brief Introduction to Decolonial Computing," *XRDS: Crossroads, The ACM Magazine for Students* 22, N°4 (2016): 16–21.

[^Refs]: 這裡我們指的是：John Maeda, *Creative Code: Aesthetics + Computation* (London: Thames & Hudson, 2004); Kylie A. Peppler and Yasmin B. Kafai, "Creative coding: Programming for personal expression," *The 8<sup>th</sup> International Conference on Computer Supported Collaborative Learning (CSCL)* 2 (2009): 76-78; Montfort, *Exploratory Programming for the Arts and Humanities*; Noah Wardrip-Fruin, *Expressive Processing: Digital Fictions, Computer Games, and Software Studies* (Cambridge, MA: MIT Press, 2012).

[^students]: 我們將形式化邏輯和概念式思維融合在一起，從而開拓進一步反思的空間。就如同我們的一位學生在 2018 年所說：「美學程式設計不僅只是程式設計入門，而是一門更富反思和批判的課程。」根據某位學生對 2019 年課程的回饋，課程結構提供了「將編碼做為實作和理論方法的實務經驗（中略）。能夠一窺程式碼幕後情況，並嘗試在數位文化脈絡下以宏觀視角進行解碼，是一件很棒的事。」

[^people]: 在此應特別感謝負責課程教學的 Magda Tyżlik-Carver 和 Christian Ulrik Andersen 兩人，以及包括 Frederik Westergaard、Nils Rungholm Jensen、Tobias Stenberg、Malthe Stavning Erslev、Ann Karring、Simone Morrison、Nynne Lucca Christianen、Ester Marie Aagaard 和 Noah Aamund 等各位助教。

[^Ranciere]: 賈克．洪席耶的《美學與政治》（The Politics of Aesthetics）(London: Continuum, 2006) 探討了美學和政治的共通之處：根據洪席耶的說法，兩者都為可思考與不可思考、可能與不可能劃定了界限。

[^Jay]: 其後的引文如下：「必須藉由批判性分析來破譯」，出自：Martin Jay, *Aesthetic Theory* (Minneapolis, MN: University of Minnesota Press, 1996), 177.

[^Benjamin]: 這點可引用本雅明的話：「當本真性的標準不再適用於藝術生產的那一刻，藝術的全部功能就反轉了。它不再以儀式為基礎，而是開始基於另一種實踐——政治。」Walter Benjamin, "The Work of Art in the Age of Mechanical Reproduction" [1936], *Selected Writings, Volume 3, 1935–1938,* Howard Eiland and Michael W. Jennings, eds. (Cambridge, MA: Belknap Press of Harvard University Press, 2002).

[^Adorno]: 阿多諾說得更好：「根據內在批評，一件成功的藝術作品是在虛假的和諧中解決客觀矛盾，但又透過在其最內在的結構中體現矛盾、純粹和不妥協，以負面方式表達和諧這個想法。」（出自 Adorno, in *Prisms* (1967), 32；引自 Jay, *Aesthetic Theory*, 179）。

[^afro]: 在這裡，黑人和原住民美學將會是很重要的補充，就如在 Ron Eglash 的《非洲分形》（African Fractals），以及對非洲未來主義（Afrofuturism）的引用中所看到的，不僅僅是將黑人的聲音重新置入種族歧視的歷史敘述中，而是設想新的、富有想像力的形式。參見：Ron Eglash's *African Fractals: Modern Computing and Indigenous Design* (New Brunswick, NJ: Rutgers University Press, 1999)，以及（舉例而言）Kodwo Eshun's "Further Considerations on Afrofuturism." *CR The New Centennial Review* 3, no.2 (2003): 287-302。  

[^Tsing]: Anna Lowenhaupf Tsing, *The Mushroom at the End of the World: On the Possibility of Life in Capitalist Ruin*s (Princeton: Princeton University Press, 2015).

[^Tsing2]: Tsing, *The Mushroom at the End of the World*, 23.

[^agre]: Philip E. Agre, "Toward a critical technical practice: Lessons learned in trying to reform AI," in Geoffrey Bowker, Susan Leigh Star, William A Turner, William Turner, Les George Gasser, eds., *Social Science, Technical Systems, and Cooperative Work: Beyond the Great Divide: * (ACM Digital Library, 1997). <https://dl.acm.org/doi/book/10.5555/549261>.

[^Chun]: 全喜卿（Wendy Chun）兼研系統工程設計和英語文學兩者，她的近期研究結合並轉化了上述兩個領域，請參閱：<https://en.wikipedia.org/wiki/Wendy_Hui_Kyong_Chun>。許煜的研究領域則兼及電腦工程和哲學，請參閱：<http://digitalmilieu.net/yuk/>。 

[^britton]: 「做中思考」的概念隱含著利用運算領域的藝術研究和科技女性主義（technofeminism）的非標準式認知方式。參見：Loren Britton, Goda Klumbyte, and Claude Draude, “Doing Thinking: Revisiting Computing with Artistic Research and Technofeminism.” *Digital Creativity* 30, N°4 (October 2, 2019): 313–28, <https://doi.org/10.1080/14626268.2019.1684322>.

[^agre1]: Agre, "Toward a critical technical practice".

[^queer]: 譯按：原文「queer」應指酷兒理論模糊化、去二元化，並具備開放性的多元特徵。

[^Williams]: Fuller, ed. *Software Studies*; Raymond Williams, *Keywords: A Vocabulary of Culture and Society* (London: Fontana, 1983); Blackwell 於 2005 年更新為：*New Keywords: A Revised Vocabulary of Culture and Society*。

[^ss]: 此工作坊的計畫描述可參閱以下庫存頁面：<https://web.archive.org/web/20100327185154/http://pzwart.wdka.hro.nl/mdr/Seminars2/softstudworkshop>。

[^placeholder]: 譯按：佔住固定位置，等待人們在裡面新增內容的符號，亦即真實意義尚待填補的空泛類別。

[^Readme]: 我們深受「Readme」系列藝術節的影響，此系列活動先後於莫斯科的 Marcros 中心（2002）、赫爾辛基的 Lume 媒體中心（2003）、奧胡斯大學和奧胡斯的 Rum46（2004）和多特蒙德的 HMKV（2005）舉辦。相關的軟體藝術儲存庫 runme.org 成立於 2003 年，有多位參與者和提交作品的人並不見得自稱為藝術家。確實，藝術這項類別本身已不足以涵蓋該領域發展出的各種創造性實踐。一年一度的數位文化藝術節 transmediale 從 2001 年開始，直至 2004 年都使用「藝術性軟體」（artistic software）或「軟體藝術」（software art）一詞來形容這些作品。許多藝術家及研究人員也撰寫、整理了軟體藝術這項類型的發展脈絡，參見：Florian Cramer, and Ulrike Gabriel, "Software Art," American Book Review, issue “Codeworks”(Alan Sondheim, ed.) (2001); Goriunova, Olga, and Alexei Shulgin. *read_me: Software Art & Cultures* (Aarhus: Aarhus Universitetsforlag, 2004); Andreas Broeckmann, "Software Art Aesthetics," *Mono* 1 (2007): 158-167。

[^codestudies]: 批判性程式碼研究（Critical Code Studies，簡稱CCS）更明確闡釋了這一點，此研究領域促進人們將原始碼當作可供批判分析的文化物件來審視。正如馬克．馬利諾所述，CCS 將程式碼看成文本材料，其主要論點是程式碼本身可以被視為「值得分析、具有豐富解釋可能性的文化文本」，此外，程式碼也讓人們得以反思「程式碼本身、編程架構、程式碼運作和特定的程式設計選擇或表達方式，以及程式碼所作用、輸出、處理和再現之物之間的關係」。參見 Mark C. Marino, “[Critical Code Studies](https://electronicbookreview.com/essay/critical-code-studies/)”, *Electronic Book Review* (December 4, 2006); "Field Report for Critical Code Studies," *Computational Culture* 4 (9<sup>th</sup> November 2014), <http://computationalculture.net/field-report-for-critical-code-studies-2014%e2%80%a8/>; Mark C. Marino, *Critical Code Studies* (Cambridge, MA: MIT Press, 2020).

[^Arns]: Inke Arns, "Read_me, run_me, execute_me: Code as Executable Text: Software Art and its Focus on Program Code as Performative Text," trans. Donald Kiraly, *MediaArtNet* (2004), <http://www.mediaartnet.org/themes/generative-tools/read_me/1/>.

[^OHP]: 欲知更多 Open Humanities Press 相關資訊，請參閱：<https://openhumanitiespress.org/>。

[^dok]: 舉例而言，dokieli 就是一個客戶端編輯器，可用以進行去中心化的文章出版、註釋和社群互動。請參閱：<https://dokie.li/>。

[^devdocs]: 參見 <https://jupyter.org/> 和 <https://devdocs.io/javascript/>。

[^OSP]: 有關開源出版（OSP）的更多資料，請參閱 <http://osp.kitchen/about>。我們對這種方法很感興趣，因為它在某種程度上打破了與傳統出版相關的分工。在傳統出版中，寫作、編輯和設計以及執行這些任務的人彼此之間壁壘分明。

[^Mackenzie]: 參見：Adrian Mackenzie’s "Preface" to *Machine Learners: Archaeology of a Data Practice* (Cambridge, MA: MIT Press, 2017); 以及 GitHub，<https://github.com/datapractice/machinelearners>。

[^Git]: 做為動態儲存庫，Git 打破了儲存與生產之間的分野。有關 Git 的更多資訊，請參閱 Matthew Fuller, Andrew Goffey, Adrian Mackenzie, Richard Mills and Stuart Sharples, "Big Diff, Granularity, Incoherence, and Production in the Github Software Repository," in Matthew Fuller, *How To Be a Geek: Essays on the Culture of Software* (Cambridge: Polity Press, 2017)。

[^graphviz]: 每章的流程圖皆由 Graphviz 程式產生，Graphviz 是用於圖形視覺化的開源軟體。所有原始碼都在 GitLab 儲存庫的「graphviz」文件夾中提供。參見 <https://graphviz.org/>。

[^setup]: 譯注：「舞台」為程式執行的背景。

[^diagram]: Stephen Morris and Orlena Gotel, "The Role of Flow Charts in the Early Automation of Applied Mathematics," *BSHM Bulletin: Journal of the British Society for the History of Mathematics* 26, N°1 (March 2011): 44–52, <https://doi.org/10.1080/17498430903449207>。

[^queer2]: 譯注：同上文所述，意指奇怪、模糊、具備開放性的特徵。

[^shiffman]: 感謝許多教育工作者以各種方式呈現本書賴以為基礎的程式碼。我們要特別提及並感謝 Daniel Shiffman 的 The Coding Train YouTube 頻道，該頻道提供出色的創意編碼教學，讓我們能以易於理解的方式學習程式設計。由於教學影片中清楚地涵蓋了基礎知識，學生也更容易吸收本書內容，進而釋放出空間，讓替代形式能夠成形。

[^dual]: 我們在此（再度）參考了 ReadMe 藝術節系列，以及 Runme.org 軟體藝術儲存庫，<http://runme.org/>。

[^papert]: 此概念由身兼數學家、電腦科學家和教育家的麻省理工學院教授 Seymour Papert 率先提出，他為一種名為 Logo 的程式語言創立了一個設計原則。參見 Seymour Papert, *Mindstorms: Children, Computers, and Powerful Ideas* (New York: Basic Books, 1980)。

[^readcode]: 批判性程式碼研究將原始碼視為一種文化文本，可進行批判性閱讀和闡釋，而不受對程式碼在技術和功能上如何運作的理解所限。參見：Marino, *Critical Code Studies*。

[^kitchen]: OSP 的主頁和筆記參見：<http://osp.kitchen/>。

[^situatedness]: 關於「處境知識」，我們參考的是唐娜・哈洛威（Donna Haraway）和其他廣義下的新唯物女性主義領域著作，亦即跳脫虛假之通用和客觀知識形式進行思考。參見：Donna Haraway, "Situated Knowledges: The Science Question in Feminism and the Privilege of Partial Perspective." *Feminist Studies* 14, N°3 (1988): 575-599。

[^teaching]: 近年來美學程式設計的課程大綱和未整理的筆記草稿請見：<https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/>。

[^barad]: 我們在此參照了新唯物女性主義，尤其是凱倫・巴拉德（Karen Barad）的思想，她橫跨電腦科學、藝術、文化研究、批判理論和軟體研究等領域，將操作指南以「合併／分開」的方式進行剪輯（cutting “together/apart”）。參見：Karen Barad, *Meeting the Universe Halfway: Quantum Physics and the Entanglement of Matter and Meaning* (Durham, NC: Duke University Press, 2007)。

[^fork]: 整個儲存庫，包括所有程式碼範例的文本和原始碼以及流程圖，目前都作為具有創用 CC 授權的開源計畫存放在 GitLab 上。若您擁有 GitLab 帳戶，我們鼓勵您分岔、複製副本或下載整個儲存庫（點按下載圖標即可）。請參閱 <https://gitlab.com/siusoon/Aesthetic_Programming_Book>。

[^gabrys]: 珍尼佛・加布里斯在《如何使用感測器工作》（How to Do Things with Sensors）的介紹中概述了她的「如何做」方法（Minneapolis, MN: University of Minnesota Press, 2019）。我們想我們所做的也是類似的事情。

[^Benjamin2]: Walter Benjamin, "The Author as Producer" [1935], quoted in Geoff Cox and Joasia Krysa, eds. *Engineering Culture: On the Author as (Digital) Producer* (New York, Autonomedia, 2005), 22.
