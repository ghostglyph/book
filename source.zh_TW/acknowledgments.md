Title: 致謝
Slug: acknowledgments
page_order: -3
sketch: p5_SampleCode.zh_TW/acknowledgement/sketch.js
download_sketch_link: https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/archive/master/Aesthetic_Programming_Book-master.zip?path=public/p5_SampleCode/acknowledgement

[RunMe：sketch](https://aesthetic-programming.gitlab.io/book/p5_SampleCode.zh_TW/acknowledgement/)

```javascript
//開啟瀏覽器主控台

let thankYou = [
  "大家彼此 \
- 一路以來共同協作",

  "臺北數位藝術中心（Digital Art Center, Taipei）\
- 對於本計畫在 2023 年的翻譯與實體工作坊支持",

  "Delfina Foundation \
- 對於本計畫在 2023 年倫敦工作坊的支持 \
- support from CSNI at LSBU, CCI at UAL",

  "我們可能已經忘記的其他人，抱歉 :p"
];

function draw() {
    frameRate(1);
    print(random(thankYou));
}
```
