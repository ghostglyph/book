//開啟瀏覽器主控台

let thankYou = [
  "大家彼此 \
- 一路以來共同協作",

  "台北數位藝術中心（Digital Art Center, Taipei）\
- 對於本計畫在 2023 年的翻譯與實體工作坊支持",

  "Delfina Foundation \
- 對於本計畫在 2023 年倫敦工作坊的支持 \
- support from CSNI at LSBU, CCI at UAL",

  "我們可能已經忘記的其他人，抱歉 :p"
];

function draw() {
    frameRate(1);
    print(random(thankYou));
}