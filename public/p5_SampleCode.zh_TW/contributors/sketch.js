let contributors = {
  working_group: {
    team: '《美學程式設計》台灣版工作小組',
    members: [
      {
        name: '孫詠怡（Winnie Soon）',
        bios: '出長於香港的孫詠怡是一位藝術家、程式設計師和研究學者，專注於數位基礎結構所涉及的文化影響，以及相關的權力不對等問題。'+
              '孫目前擔任倫敦藝術大學創意計算學院的課程主管／高級講師。'
      }, {
        name: '傑夫．考克斯（Geoff Cox）',
        bios: '傑夫．考克斯是倫敦南岸大學藝術與計算文化學教授，同時擔任網絡影像研究中心（CSNI）共同主任，並兼任於奧胡斯大學。'
      }, {
        name: '李紫彤（Lee Tzu Tung）',
        bios: '李紫彤為觀念藝術家、策展人。台灣大學學士、麻省理工學院（MIT）、芝加哥藝術學院(SAIC) 雙碩士，'+
              '作品在世界各地博物館、藝廊中皆有展出。結合人類學研究與政治行動，'+
              '李紫彤擅以參與式計畫，邀請參與者成為計畫的共同創作者，一齊挑戰、解殖當代權威。'+
              '他也是許多政治活動召集人、科技與藝術合作團體《微米宇宙》、《藝術松》創始人、藝術與人類學雙年展《感野》策展人。'+
              '詳見： www.tzutung.com'
      }, {
        name: '李佳霖（Lee Chia-Lin）',
        bios: '畢業於台大外文系、中國美術學院當代藝術與社會思想研究所，'+
              '研究與實踐關注方向為數位時代的文化研究、媒體理論與藝術創作。'+
              '目前就讀台北藝術大學美術系博士班，同時擔任自牧文化負責人，製作展覽與出版圖書。'
      }, {
        name: '徐詩雨（Shih-yu Hsu）',
        bios: '徐詩雨畢業於國立中央大學通訊工程所與紐約大學視覺藝術行政研究所，'+
              '現為獨立研究者與自由撰稿人，同時擔任讀書會《軟爛》的成員，'+
              '2018 - 2021 年擔任台北當代藝術中心策展人，研究的領域包括圖像、媒介與新物質女性主義。'+
              '她同時也是第八屆台灣錄像藝術展《生／活在一起》共同策展人。'
      }, {
        name: '林彥璋（Andrew Lin）',
        bios: '關注開源、自造文化、科技藝術。現為網通軟韌體工程師及假日自造者。'
      }
    ]
  },
  translation: {
    translators: [{
      name:   '郭乙萱（Yi-Hsuan Kuo）',
      bios:   '台大外文/財金系學士，英國劍橋大學東亞所碩士，平時關注文化歷史、旅遊美食與藝術創作，目前旅居英國，擔任自由譯者、撰稿人兼康河撐篙船夫。'
      }
    ],
    notes:    '全書由[自牧文化](https://zimu-culture.com/about/)委託譯者郭乙萱翻譯',
    license:  '譯稿依照原書授權條款 CC-BY-SA 方式釋出'
  },
  co_editors: [
              '林玟伶',
              '洪寶儀'
  ],
  workshop: [
    {
      name:     '《美學程式設計》分岔工作坊 V.1',
      date:     '2023-08-05',
      location: '臺北數位藝術中心',
      participants: [
                '孫竹均',
                '楊大毅',
                '吳耳呈',
                '王秉中',
                'KEN',
                'Sylvia',
                '子鈺',
                'U0',
                'Renyu',
                '涼枕',
                'Kiki'
      ]
    }, {
      name:     '《美學程式設計》分岔工作坊 V.2',
      date:     '2023-09-13',
      location: '倫敦戴芬娜基金會（Delfina Foundation）',
      participants: [
                'Yuan9lifes',
                'Anders Aarvik',
                '寇佳瑛',
                '李伊寧(Erin Li)',
                'April Chu',
                '汪骄阳',
                '宋永琪（Vicky Sung）',
                '廖育萱 (Sunni Liao)',
                'Chloe Karnezi',
                'Patrick Harrison (何波)',
                '王金尧（Jinyao Wang）',
                'Julian Tapales',
                'Esther Yi-Chun Lin 林怡君'
      ]
    }
  ]
};

function setup() {
  for (let i =0; i < contributors.working_group.members.length; i++) {
    print(contributors.working_group.members[i].name);
  }
  
  for (let i =0; i < contributors.translation.translators.length; i++) {
    print(contributors.translation.translators[i].name);
  }
  
  for (let i =0; i < contributors.co_editors.length; i++) {
    print(contributors.co_editors[i]);
  }
  
  for (let i =0; i < contributors.workshop.length; i++) {
    for (let j =0; j < contributors.workshop[i].participants.length; j++) {
      print(contributors.workshop[i].participants[j]);
    }
  }
}
