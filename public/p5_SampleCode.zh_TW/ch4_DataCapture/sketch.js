/*Interacting with captured data: Mouse, Keyboard, Audio, Web Camera
check:
1. sound input via microphone: https://p5js.org/examples/sound-mic-input.html
2. dom objects like button
3. p5.sound library:
https://github.com/processing/p5.js-sound/blob/master/lib/p5.sound.js
4. Face tracking library: https://github.com/auduno/clmtrackr
5. p5js + clmtracker.js: https://gist.github.com/lmccart/2273a047874939ad8ad1
note: the audio doesn't work on Firefox 96, the temp fix is to import older ver.
See: https://aesthetic-programming.gitlab.io/book/ > #4 with older libraries
*/
let button;
let mic;
let ctracker;
let capture;

function setup() {
  createCanvas(640, 480);
  // 網路攝影機擷取
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();

  //音訊擷取
  mic = new p5.AudioIn();
  mic.start();

  //設定臉部追蹤器
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  //以 CSS 設計按讚按鈕
  button = createButton('like');
  button.style("display", "inline-block");
  button.style("color", "#fff");
  button.style("padding", "5px 8px");
  button.style("text-decoration", "none");
  button.style("font-size", "0.9em");
  button.style("font-weight", "normal");
  button.style("border-radius", "3px");
  button.style("border", "none");
  button.style("text-shadow", "0 -1px 0 rgba(0, 0, 0, .2)");
  button.style("background", "#4c69ba");
  button.style(
    "background","-moz-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "background","-webkit-gradient(linear, left top, left bottom, \
      color-stop(0%, #3b55a0))");
  button.style(
    "background","-webkit-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "background","-o-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "background","-ms-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "background","linear-gradient(to bottom, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "filter","progid:DXImageTransform.Microsoft.gradient \
    ( startColorstr='#4c69ba', endColorstr='#3b55a0', GradientType=0 )");
  //滑鼠擷取
  button.mouseOut(revertStyle);
  button.mousePressed(change);
}
function draw() {
  //取得音訊資料，亦即整體音量（0 到 1.0 之間）
  let vol = mic.getLevel();
  /*將麥克風音量與按鈕大小對應，
    欲查閱對應函式，請至：https://p5js.org/reference/#/p5/map */
  button.size(floor(map(vol, 0, 1, 40, 450)));

  //透過影像濾鏡將擷取下來的視訊繪製在螢幕上 
  image(capture, 0,0, 640, 480);
  filter(INVERT);

  let positions = ctracker.getCurrentPosition();
  //查看網路攝影機追蹤是否可用
  if (positions.length) {
     //編號 60 的點即為嘴部區域
    button.position(positions[60][0]-20, positions[60][1]);
    /*走訪並描繪臉部所有重要的點
      （請見：https://www.auduno.com/clmtrackr/docs/reference.html）*/
    for (let i = 0; i < positions.length; i++) {
       noStroke();
       //使用 alpha 值上色
       fill(map(positions[i][0], 0, width, 100, 255), 0, 0, 120);
       //在每個位置點繪製圓形
       ellipse(positions[i][0], positions[i][1], 5, 5);
    }
  }
}

function change() {
  button.style("background", "#2d3f74");
  userStartAudio();
}
function revertStyle(){
  button.style("background", "#4c69ba");
}
//鍵盤擷取
function keyPressed() {
  //spacebar - check here: http://keycode.info/
  if (keyCode === 32) {
    button.style("transform", "rotate(180deg)");
  } else {   //鍵盤擷取
    button.style("transform", "rotate(0deg)");
  }
}
