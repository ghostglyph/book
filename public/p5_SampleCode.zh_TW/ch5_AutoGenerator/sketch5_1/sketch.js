/* Inpsired by Langton's Ant (1986) by Chris Langton and
Daniel Shiffman: https://tinyurl.com/ShiffmanLangton
other ref: learning: 2d array by Daniel Shiffman:
https://www.youtube.com/watch?v=OTNpiLUSiB4

Modified by Winnie Soon
Logic:
1. draw a grid with your desired size (grid_space) via initializing
a grid structure based on width and height, columns and width
2. set all the cell states as 0 (where 0 =off/white, 1 = on/black)
3. set initial (current) x, y position and direction in the setup()
4. logic starts - in the draw():
  - based on the current position to check if the cell
    hits the edges (width and height)
  - check the current state of the cell against the rules
  (2 rules in total and those also define the on/off state of the cell)
  - change color of the cell
  - update corresponding/latest ant's direction + state
  - move to next cell and loop again within #4
  - *'grid_space' needs to be dividable, as an integer, by the width and height of the canvas
*/

 //例：4、5 和 10 需要能被畫布的寬和高所整除 
let grid_space = 5;
let grid =[]; //開／關狀態 
 //上述程式碼用於繪製網格
let cols, rows;
//目前位置以行和列表示，而非實際像素
let xPos, yPos;
//螞蟻目前所朝的方向
let dir;
const antUP = 0;
const antRIGHT = 1;
const antDOWN = 2;
const antLEFT = 3;
let offColor;
let onColor;

function setup() {
  createCanvas(1000, 700);
  offColor = color(255);  //「關閉」顏色設定 
  onColor = color(0); //「開啟」顏色設定
  background(offColor);
  grid = drawGrid();
  xPos = floor(cols/2);  //初始 x 座標位置（整數） 
  yPos = floor(rows/2); //初始 y 座標位置（整數）
  dir = antUP; //初始面朝方向
  frameRate(20);
}
function draw() {
  /*為了讓每個影格運行得更快，
  　請試著更改數字，例如將之改成 1，而非維持 100*/
  for (let n = 0; n < 100; n++) {
   checkEdges();
   let state = grid[xPos][yPos];
   //檢查目前所在格子的狀態
   //規則一
   if (state == 0) {
    dir++;  //右轉 90°
    grid[xPos][yPos] = 1; //將目前格子狀態改設為「開」
    fill(onColor);  //隨設定改變而來的顏色改變
    if (dir > antLEFT) {
      dir = antUP;  //重置計數器
    }
   //規則二
   }else{
    dir--;  //左轉 90°
    grid[xPos][yPos] = 0; //將目前格子狀態改設為「關」
    fill(offColor);  //隨設定改變而來的顏色改變
    if (dir < antUP) {
      dir = antLEFT; //重置計數器
    }
   }
   rect(xPos*grid_space, yPos*grid_space, grid_space, grid_space);
   nextMove();
  }
}
function drawGrid() {
  cols = width/grid_space;
  rows = height/grid_space;
  let arr = new Array(cols);
  for (let i = 0; i < cols; i++) { //列數
    arr[i] = new Array(rows); //2D 陣列
    for (let j = 0; j < rows; j++){ //行數
      let x = i * grid_space; //實際 x 座標
      let y = j * grid_space; //實際 y 座標
      stroke(0);
      strokeWeight(1);
      noFill();
      rect(x, y, grid_space, grid_space);
      arr[i][j] = 0;  //將關閉狀態和顏色指派給各個格子
    }
  }
  return arr; //回傳值為格子狀態的函式
}
function nextMove () {
  //檢查下一步要朝哪個方向，並設定好新的目前方向
  if (dir == antUP) {
    yPos--;
  } else if (dir == antRIGHT) {
    xPos++;
  } else if (dir == antDOWN) {
    yPos++;
  } else if (dir == antLEFT) {
    xPos--;
  }
}
function checkEdges() {
  //檢查寬度和高度邊界
  if (xPos > cols-1) { //碰到右側邊緣
    xPos = 0;    //回到左側
  } else if (xPos < 0) {  //碰到左側邊緣
    xPos = cols-1;  //至右側邊緣
  }
  if (yPos > rows-1) { //碰到下側邊緣
    yPos = 0; //回到上方
  } else if (yPos < 0) { //碰到上方邊緣
    yPos = rows-1;  //至下方
  }
}
