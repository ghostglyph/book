/*靈感來自大衛・萊因福特（David Reinfurt）的作品《Multi》*/
let moving_size = 50;
let static_size = 20;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(15);
}

function draw() {
  //背景
  background(random(230, 240));
  //左邊
  noStroke()
  fill(0);
  rect(97, 169, 79, 12);

  //右邊
  rect(365, 184, 20, 15);
  fill(20, 20, 120);

  beginShape();
  vertex(365, 199);
  vertex(385, 199);
  vertex(372, 216);
  vertex(358, 216);
  endShape(CLOSE);

  //底部
  noFill();
  stroke(130);
  strokeWeight(2);
  ellipse(255, 350, static_size, static_size);

  //滑鼠互動
  stroke(180);
  ellipse(mouseX, mouseY, moving_size, moving_size);

  if (mouseIsPressed) {
    static_size = floor(random(5, 20));
 }
}
